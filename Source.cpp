#include <iostream>

class Animal
{
public:
	virtual void Voice() = 0;
};

class Dog : public Animal
{
	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal
{
	void Voice() override
	{
		std::cout << "Meow!\n";
	}
};

class Fox : public Animal
{
	void Voice() override
	{
		std::cout << "Gering-ding-ding-ding-dingeringeding!\n";
	}
};

class PepeTheFrog : public Animal
{
	void Voice() override
	{
		std::cout << "REEEEEeeeee!\n";
	}
};

int main()
{
	int size = 4;
	Animal** AnimalArray = new Animal*[size];

	AnimalArray[0] = new Dog;
	AnimalArray[1] = new Cat;
	AnimalArray[2] = new Fox;
	AnimalArray[3] = new PepeTheFrog;

	for (int i = 0; i < size; i++)
	{
		AnimalArray[i]->Voice();
	}
	return 0;
}